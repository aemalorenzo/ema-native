import React, { Component } from 'react';
import { Provider, connect } from 'react-redux';
import thunk from 'redux-thunk';
import AppWithNavigationState from './src/containers/app';

// Store configuration
import configureStore from '@configuration';

// store
const store = configureStore();

export default class App extends Component {
  render(){
    return (
      <Provider store={store}>
        <AppWithNavigationState/>
      </Provider>
    );
  }
}
import { connect } from 'react-redux'
import Component from '../components/app'

const mapStateToProps = (state, ownProps) => {
  return {
    nav: state.nav
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    dispatch
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Component)
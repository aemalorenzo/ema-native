
import React, { Component } from 'react';
import { 
  TouchableOpacity,
  StatusBar,
  View,
  Text,
} from 'react-native';

export default class Welcome extends Component {
  static navigationOptions = {
    title: 'EmaNative',
  };

  render() {
    return (
      <View>
        <StatusBar
          backgroundColor="white"
          barStyle="dark-content"/>
        <View style={styles.container}>
          <Text>
            Hey hola! 
          </Text>
        </View>  
      </View>  
    );
  }
}

const styles = {
  container: {
    padding: 16,
  },
};
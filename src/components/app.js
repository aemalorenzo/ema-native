import React, { Component } from 'react'
import { BackAndroid } from 'react-native'
import { addNavigationHelpers } from 'react-navigation';
import AppNavigator from './appNavigator'

export default class AppWithNavigationState extends Component {

    shouldCloseApp(nav) {
      return nav.index == 0;
    }
    
    componentDidMount() {
      BackAndroid.addEventListener('backPress', () => {
        const {dispatch, nav} = this.props;
        if (this.shouldCloseApp(nav)) return false
        dispatch({ type: 'Navigation/BACK' })
        return true
      })
    }

    componentWillUnmount() {
      BackAndroid.removeEventListener('backPress')
    }

    render() {
      return (
        <AppNavigator navigation={addNavigationHelpers({
          dispatch: this.props.dispatch,
          state: this.props.nav,
        })} />
      );
    }
  }
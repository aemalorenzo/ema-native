import {AppRegistry} from 'react-native';
import React from 'react';
import { StackNavigator } from 'react-navigation';
// scenes
import Welcome from '../components/scenes/welcome';
import About from '../components/scenes/about';

export default StackNavigator({
  'welcome': { screen: Welcome },
  'about': { screen: About },
},{
  headerMode: 'screen',
  initialRouteName: 'welcome',
});
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducer from "@reducers";

export default (initialState: Object = {}) => {

  const store = createStore(
    reducer,
    initialState,
    applyMiddleware(thunk),
  );

  if (__DEV__ && module.hot) {
    module.hot.accept(() => {
      const nextRootReducer = require('@reducers').default;
      store.replaceReducer(nextRootReducer);
    });
  }

  return store;
}
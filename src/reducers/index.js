
import { combineReducers } from 'redux';

// reducers
import nav from './shared/navigator';

export default combineReducers({
  nav,
});